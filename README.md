![screenshot](/screenshot.png)

Black night theme

Without ad

Without "Services" dropdown at top-right.

without bottom-left buttons to other Yandex services

without translation button help

For use with [Stylus](https://github.com/openstyles/stylus) for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Firefox](https://addons.mozilla.org/firefox/addon/styl-us).

Install theme from https://userstyles.world/style/14779/developer-mozilla-org-feb-2024
